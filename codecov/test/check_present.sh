#env bash
set -euo pipefail
echo $0

output=`/codecov --help || true`
echo $output

[[ $output = Usage:\ codecov* ]] || exit 3
