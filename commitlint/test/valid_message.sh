#!env bash
set -euo pipefail
echo $0

echo "fix: a valid message" | commitlint -g ../.commitlint.config.js

if [ $? -gt 0 ]; then
  echo "Commitlint denies a valid commit message!"
  exit 1
fi
