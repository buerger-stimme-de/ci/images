#env bash
set -euo pipefail
echo $0

output=`commitlint -g ../.commitlint.config.js --version`
echo $output

[[ $output = @commitlint* ]] || exit 3
