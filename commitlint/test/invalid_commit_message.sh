#!env bash
echo $0

echo "fix: An invalid message" | commitlint -g ../.commitlint.config.js

if [ $? -eq 0 ]; then
  echo "Commitlint does not recognize an invalid message!"
  exit 2
fi

exit 0
