#env bash
set -euo pipefail
echo $0

semantic-release --version

# no special value. Will exit automatically if an error occurrs
