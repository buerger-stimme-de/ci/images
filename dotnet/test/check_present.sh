#env bash
set -euo pipefail
echo $0

dotnet --version

# no special value. Will exit automatically if an error occurrs
