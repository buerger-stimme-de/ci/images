# Images

Mono repository holding all image with pre-installed dependencies which are used in out pipelines.

## Commitlint
To check/verify the merge request.

## ESLint
Used to lint Javascript code.

## DotNet
The main image to build a DotNet application.

## Kaniko
Used to build Docker images.

## Semantic-Release
An image used to create a release for the software.
