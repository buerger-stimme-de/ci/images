#env bash
set -euo pipefail
echo $0

/kaniko/executor version

# no special value. Will exit automatically if an error occurrs
