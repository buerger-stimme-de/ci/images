#env bash
set -euo pipefail
echo $0

output=`eslint --version`
echo $output

[[ $output = v* ]] || exit 1
